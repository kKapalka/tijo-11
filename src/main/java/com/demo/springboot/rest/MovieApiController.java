package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

@Controller
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    @RequestMapping(value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies() {

        LOGGER.info("--- get movies");

        List<MovieDto> movies = new ArrayList<>();

        String csvFile="C:\\Users\\student\\Desktop\\movies\\movies\\movies.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] moviedata = line.split(cvsSplitBy);
                movies.add(new MovieDto(Integer.parseInt(moviedata[0].trim()),moviedata[1],Integer.parseInt(moviedata[2].trim()),moviedata[3]));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        // TODO: Usluga powinna realizowac nastepujace zadania:
        // TODO: Prosze odczytac zawartosc pliku movies.csv
        // TODO: Prosze zapisac wszystkie dane z pliku movies.csv do listy znajdujacej sie w klasie MovieListDto
        // TODO: Po skonczonej implementacji prosze przetestowac w przegladarce usluge pod adresem http://127.0.0.1:8080/movies
        // TODO: Nastepnie prosze otworzyc plik movies.html

        return new MovieListDto(movies);
    }
}
